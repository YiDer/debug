package com.engine.wei.debug;

import android.app.Application;

import com.engine.wei.base.FrameManager;

/**
 * 作者:赵若位
 * 时间:2021/11/18 22:12
 * 功能:
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FrameManager.init(this);
    }
}
