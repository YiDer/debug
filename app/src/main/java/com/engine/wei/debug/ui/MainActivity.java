package com.engine.wei.debug.ui;

import com.engine.wei.base.mvp.presenter.BasePresenter;
import com.engine.wei.base.ui.activity.BaseActivity;
import com.engine.wei.debug.databinding.ActivityMainBinding;

/**
 * 作者:赵若位
 * 时间:2021/9/3 14:39
 * 功能:
 */
public class MainActivity extends BaseActivity<ActivityMainBinding, BasePresenter> {

    @Override
    public ActivityMainBinding createViewBinding() {
        return ActivityMainBinding.inflate(getLayoutInflater());
    }

    @Override
    public void initView() {
    }

    @Override
    public void initData() {

    }
}
